// On peut déclarer un tableau de cette manière
let mesNombres = [2, 6, 7, 8, 87, 12.6574];

// Ou de cette manière s'il s'agit de chaînes de caractères
let mesMots = ['rouler', 'rabattre', 'plier', 'emmagasiner', 'courber', 'raccourcir', 'tordre'];

//Ou encore déclarer un tableau vide que l'on va remplir par après, avec une boucle par exemple.
let monTableau = [];

function setup() {
    createCanvas(windowWidth, windowHeight);
    frameRate(5);
}

function draw() {
  randomIndex = floor(random(mesMots.length));
  text(mesMots[randomIndex], random(width), random(height));
}
function mousePressed() {
  mesMots.push(hour()+":"+minute()+":"+second());
}

/*
push() : ajoute un élément à la fin du tableau
unshift() : ajoute un élément au début du tableau
splice() : supprime un ou plusieurs éléments d'un tableau
append()
*/