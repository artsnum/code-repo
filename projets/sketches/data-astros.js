let result;
let posy=100;

function preload() {
  result = loadJSON("http://api.open-notify.org/astros.json", 'jsonp')
}

function setup() {
  createCanvas(800,800);
  noLoop();
  background(200);
  print(result);
  for(var i=0;i<result.people.length;i++) {
    text(result.people[i].name, 100, posy);
    text(result.people[i].craft, 100, posy+17);
    posy= posy+50;
  }
}