let result;
function preload() {
    result = loadJSON("./assets/data/colors.json");
}
function setup() {
    noCanvas();
    print(result.colors.length);
    let cont = createDiv().addClass("container");
    for (var i = 0; i < result.colors.length; i++) {
        createP(result.colors[i].color).style("background", result.colors[i].hex).addClass("color").parent(cont);
    }
}