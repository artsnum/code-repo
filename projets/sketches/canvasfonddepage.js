let banana;

function setup() {
    createCanvas(windowWidth, windowHeight);
    background("#d9d8d4");
    imageMode(CENTER);
    banana = loadImage('./assets/img/banana-sticky.png');
}

function draw() {
   if(mouseIsPressed) {
       image(banana,mouseX,mouseY);
   }
}
