/*
https://www.reddit.com/r/worldnews.json
https://github.com/toddmotto/public-apis
https://api.abalin.net/
*/

let result;
let posy=100;
function preload() {
  result = loadJSON("https://www.reddit.com/r/worldnews.json")
}

function setup() {
  noCanvas();
  noLoop();
  background(200);
  print(result.data.children);
  //print(result.data);
  for(var i=0;i<result.data.children.length;i++) {
    createP(result.data.children[i].data.title, 100, posy);
    posy= posy+50;
  }
}