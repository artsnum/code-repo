var mesImages = [];
var rand;
var nombre = 8;

function preload() {
    for (var i = 0; i < nombre; i++) {
        mesImages[i] = loadImage("./assets/img/arrayofimages/image_" + i + ".jpg");
    }
}

function setup() {
    createCanvas(windowWidth, windowHeight);
}

function draw() {}

function mouseClicked() {
    rand = floor(random(nombre));
    image(mesImages[rand], mouseX - (mesImages[rand].width / 2), mouseY - (mesImages[rand].height / 2));
    print(rand);
}