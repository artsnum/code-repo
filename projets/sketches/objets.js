var machins = [];

function setup() {
    createCanvas(windowWidth, windowHeight);
    colorMode(HSB, 360, 100, 100, 100);
    for (var i = 0; i < 3; i++) {
        machins[i] = new Machin(random(width), random(height), random(20), random(40));
    }
}

function draw() {
    background(250, 40, 100, 15);
    for (var i = 0; i < machins.length; i++) {
        machins[i].display();
        machins[i].deplace();
        machins[i].changeCouleur();
    }
}

function keyPressed() {
    noLoop();
}

class Machin {
    constructor(x, y, vitesse, teinte) {
        this.x = x,
        this.y = y,
        this.vitesseX = vitesse,
        this.vitesseY = vitesse,
        this.teinte = teinte
    }
    display() {
        fill(this.teinte, 100, 100, 50);
        rect(this.x, this.y, 100, 50, 50, 50, 5, 5);
    }
    deplace() {
        if (this.x > (width - 100) || this.x < 0) {
            this.vitesseX *= -1;
            console.log("Je rebondis!");
        }
        if (this.y > (height - 50) || this.y < 0) {
            this.vitesseY *= -1;
            console.log("Je rebondis!");
        }
        this.x += this.vitesseX;
        this.y += this.vitesseY;
    }
    changeCouleur() {
        this.teinte += random(1);
    }
}