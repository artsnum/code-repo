// https://p5js.org/reference/#/p5/loadJSON
// Outside of preload(), you may supply a callback function to handle the object

let input, button, weather, url, name, madata;

function setup() {
  noCanvas();

  input = createInput();
  input.position(20, 65);

  button = createButton('submit');
  button.position(input.x + input.width, 65);
  button.mousePressed(getWeather);

  weather = createElement('h2', 'Choisis une ville (en anglais)');
  weather.position(20, 5);

  textAlign(CENTER);
  textSize(50);
}
function draw() {}
function getWeather() {
    name = input.value();
    print(name);
  url = "https://samples.openweathermap.org/data/2.5/weather?q="+name+",uk&appid=b6907d289e10d714a6e88b30761fae22";
  madata = loadJSON(url, 'jsonp');
  print(madata.weather.id);
}

function gotWeather(weather) {
    print(weather);
}
