// https://p5js.org/reference/#/p5/loadJSON
// Outside of preload(), you may supply a callback function to handle the object

let bouton;

function setup() {
  noCanvas();
  input = createInput();
  bouton = createButton("Get weather!");
  bouton.mousePressed(gotWeather);
}

function draw() {}

function gotWeather() {
    let name = input.value();
    let url = 'https://api.openweathermap.org/data/2.5/weather?q='+name+'&APPID=0cac417d73d2181254b1cd3075dad589';
    loadJSON(url, displayWeather);
}

function displayWeather(dispweather) {
 createP(dispweather.base);
 createP("weather: "+dispweather.weather[0].description);
}