let dia = 100;
let moon;
let angle;
function preload() {
  moon = loadImage("https://www.solarsystemscope.com/textures/download/2k_venus_surface.jpg");
}

function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);
  //rectMode(CENTER);
  noStroke();
  angle = 0;
  smooth();
  
}

function draw() {
  background(0,0,0);
  directionalLight(255,255,255,.75,0,0);
  texture(moon);
  rotateY(angle);
  //specularMaterial(255);
  sphere(dia);
  angle+=0.01;
}