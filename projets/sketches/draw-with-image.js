let img;
let multiplicateur;
let largeur, hauteur;

function preload() {
    img = loadImage('img/animal-aquarium-aquatic-small.jpg');
}
  
function setup() {
    createCanvas(windowWidth, windowHeight);
    background(51);
    largeur = 300;
    hauteur = 200;
    multiplicateur = 1.001;
}
function draw() {
    image(img, mouseX,mouseY, largeur, hauteur);
    if (keyIsPressed) {
        if(keyCode === UP_ARROW) {
            largeur = largeur*multiplicateur;
            hauteur = hauteur*multiplicateur;
        } else if(keyCode === DOWN_ARROW) {
            largeur = largeur/multiplicateur;
            hauteur = hauteur/multiplicateur;
        }
    }
}
/*
function keyPressed() {
    if(keyCode === UP_ARROW) {
        largeur = largeur*multiplicateur;
        hauteur = hauteur*multiplicateur;
    }
}
*/