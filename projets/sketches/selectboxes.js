var monBouton, selectSujets, selectVerbes, selectComplements, montexte, monCanvas;
var sujets = ["Les enfants", "Les cerises","Les crayons"];
var verbes = [" mangent ", " écrabouillent "," redéfinissent "];
var complements = ["les contours.", "les têtes à claques.","les cailloux."];

function setup() {
  noCanvas();
  selectSujets = createSelect();
  for(var i=0;i<sujets.length; i++) {
    selectSujets.option(sujets[i]);
  }

  selectVerbes = createSelect();
  for(var i=0;i<verbes.length; i++) {
    selectVerbes.option(verbes[i]);
  }

  selectComplements = createSelect();
  for(var i=0;i<complements.length; i++) {
    selectComplements.option(complements[i]);
  }
  monBouton = createButton('Afficher la phrase');
  monBouton.mouseClicked(mySelectEvent);
}

function mySelectEvent() {
  var itemSujet = selectSujets.value();
  var itemVerbe = selectVerbes.value();
  var itemComplement = selectComplements.value();
  background(200);
  createP(itemSujet + itemVerbe + itemComplement);
}