let monTableau = [4, 7, 123, 876.870987643, 'Une chaîne de caractère'];

function setup() {
    createCanvas(windowWidth, windowHeight);
    textSize(30);
    for(var i=0;i<monTableau.length;i++) {
        text(monTableau[i], 100, 50*i);
    }
}