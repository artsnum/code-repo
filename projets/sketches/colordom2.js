let result;
function preload() {
    result = loadJSON("./assets/data/colors2.json");
}
function setup() {
    noCanvas();
    print(result.length);
    for (let i = 0; i < 5; i++) {
        createP(result[i].name).style("background", result[i].hex);
      }
}