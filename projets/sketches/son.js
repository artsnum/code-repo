let son;
let boutonplay, boutonstop;
  
function setup() {
    noCanvas();
    // On crée les boutons et on leur assigne une classe pour les styler
    boutonplay = createButton("Play").class('play');
    boutonstop = createButton("Stop").class('pause');
    // Charge le fichier son (attention au chemin d'accès)
    son = loadSound('./assets/sounds/503232__gis-sweden__electronic-minute-no-339-one-osc-bass-and-drum-machine-loop.wav');
}

function draw() {
    // On appelle certaines fonction (voir plus bas) quand on clique sur les boutons
    boutonplay.mousePressed(joueMonSon);
    boutonstop.mousePressed(stopMonSon);
}

function joueMonSon() {
    son.play();
}
function stopMonSon() {
    son.pause();
}