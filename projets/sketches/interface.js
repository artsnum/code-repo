// Voir les programming posters et le studio machin de tim

let rSlider, gSlider, bSlider;

function setup() {
    // On crée un canvas pleine page
    createCanvas(windowWidth, windowHeight);
    textSize(15);
    noStroke();

    // On crée les sliders, on les paramètre et on les place
    sizeSlider = createSlider(0, 600, 300);
    sizeSlider.position(windowWidth - 200, 20);
    rSlider = createSlider(0, 255, 100);
    rSlider.position(20, 20);
    gSlider = createSlider(0, 255, 0);
    gSlider.position(20, 50);
    bSlider = createSlider(0, 255, 255);
    bSlider.position(20, 80);
}

function draw() {
    // On récupère leur valeur à chaque boucle (on pourrait utiliser change() )
    const sz = sizeSlider.value();
    const r = rSlider.value();
    const g = gSlider.value();
    const b = bSlider.value();
    background(r, g, b);
    text('red', rSlider.x * 2 + rSlider.width, 35);
    text('green', gSlider.x * 2 + gSlider.width, 65);
    text('blue', bSlider.x * 2 + bSlider.width, 95);
    ellipse(width / 2, height / 2, sz, sz);
}