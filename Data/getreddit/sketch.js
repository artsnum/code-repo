let input, howmany, trigger;

function setup() {
    noCanvas();
    input = select('#subr');
    howmany = select('#limitSub');
    trigger = select('#print');
    trigger.mousePressed(pressed);
}

function pressed() {
    let inputvalue = input.value();
    loadJSON('https://www.reddit.com/r/'+inputvalue+'.json', gotData);
}

function gotData(data) {
    let subRed = data;
    let howmanyvalue = howmany.value();
    for(let i=0; i<howmanyvalue; i++) {
        let itemTitle = subRed.data.children[i].data.title;
        let itemAuthor = subRed.data.children[i].data.author;
        let itemScore = subRed.data.children[i].data.score;
        let thisparent = createDiv().id(i);
        createP(itemTitle).parent(thisparent).addClass('subredtitle');
        createP('Author: '+itemAuthor+', Score: '+itemScore).parent(thisparent).addClass('metadata');
    }
}