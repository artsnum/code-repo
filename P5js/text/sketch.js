let alice;

function preload() {
  alice = loadFont('assets/Alice-Regular.ttf');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  textFont(alice);
  textSize(width / 20);
  textAlign(CENTER, CENTER);
  background(255);
}

function draw() {
    fill(0,10);
    text('Un texte', mouseX, mouseY);
}