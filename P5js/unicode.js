fontsize = 10;

function setup() {
  createCanvas(windowWidth, windowHeight);
  textSize(fontsize);
  fill(255);
  background(0);

  let gap = 12;
  let margin = 0;
  let counter = 0; //Où commencer...

  for (let y = 0; y < height - gap; y += gap) {
    for (let x = 0; x < width - gap; x += gap) {
      let letter = char(counter);
      text(letter, x, y);
      counter++;
    }
  }
}
