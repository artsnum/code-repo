let img, rand, nbrimages;

function setup() {
    noCanvas();
    frameRate(4);
    nbrimages = 582;
}

function draw() {
    rand = int(random(nbrimages));
    img = createImg("img/image" + rand + ".png", 'Aï');
    img.position(mouseX - (img.width / 2), mouseY - (img.height / 2));
}