// gui params
var myColor = '#ff0000';
var long = 20;
var haut = 20;
var posx, posy;
var overlapx;
var overlapy;
var nombre = 15;
var cote;

var gui;

function setup() {
    createCanvas(windowWidth, windowHeight);
    sliderRange(0, 90, 1);
    overlapx = 5;
    overlapy = 5;
    cote = width / nombre;
    gui = createGui('p5.gui');
    gui.addGlobals('myColor', 'cote', 'overlapx', 'overlapy');
    posx = cote / 2;
    posy = 0;
    //noStroke();
    noLoop();
    blendMode(EXCLUSION);
    rectMode(CENTER);
}


function draw() {
    //  background(255);
    fill(myColor);
    posy = 0;
    while (posy <= height) {

        posx = cote / 2;
        while (posx <= width) {
            rect(posx, posy, cote, cote);
            posx += cote - overlapx;
        }
        posy += cote - overlapy;
    }



    /*if (posx >= width) {
        posx = 0;
        posy = posy + haut - overlap;
    }
    rect(posx, posy, long, haut);
    posx += long - overlap;*/

}

function keyIsPressed() {
    save('monpattern-' + hour() + minute() + second() + ".png");
}

// dynamically adjust the canvas to the window
function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}