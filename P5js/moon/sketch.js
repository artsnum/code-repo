
let dia = 300;
let moon;
let cote = 300;
let cam;
let angle1, angle2;
function preload() {
  moon = loadImage("assets/containers2.jpg");
  //https://theloadstar.com/wp-content/uploads/%C2%A9-Binkski-containers_9754977.jpg
}

function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);
  //rectMode(CENTER);
  noStroke();
  angle1 = 0;
  angle2 = 0;
  cote=width/4;
  smooth();
  /*cam = createCapture(VIDEO);
  cam.hide();*/
}

function draw() {
  background(0,0,0);
  translate(0,0,mouseX);
  push();
  translate(cote-width/2, cote-height/2);
  
    ambientLight(255);
    texture(moon);
    rotateX(angle1*0.1);
    rotateY(angle1);
    rotateZ(angle1);
    //specularMaterial(255);
    //sphere(dia);
    box(cote);
  pop();
  push();
  translate((cote*2)-width/2, cote-height/2);


    texture(moon);
    rotateX(angle2*0.1);
    rotateY(angle2);
    rotateZ(angle2);
    //specularMaterial(255);
    //sphere(dia);
    box(cote);
  pop();
  angle1+=0.002;
  angle2+=0.0017;
}