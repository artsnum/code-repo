int x = 0;
int r = 0;
int v = 0;
int b = 0;
int step = 0;
color maCouleur;

void setup() {
  size(5000,1000);
  background(230,190,20);
  
}
void draw() {
  maCouleur = color(r,v,b);
  stroke(maCouleur);
  line(x,0,x,height);
  if(step == 0) {
  if (frameCount%2==0) {
      r = r+1;
    } else if (frameCount % 3 ==0) {
      v = v+1;
    } else if (frameCount % 5 == 0) {
      b= b+1;
    }
  }
  if(step == 1) {
    if (frameCount%2==0) {
      v = v+1;
    } else if (frameCount % 3 ==0) {
      b = b+1;
    } else if (frameCount % 5 == 0) {
      r= r+1;
    }
  }
  if(step == 2) {
    if (frameCount%2==0) {
      b = b+1;
    } else if (frameCount % 3 ==0) {
      r = r+1;
    } else if (frameCount % 5 == 0) {
      v= v+1;
    }
  }
  x = x+1;
  if (x==width) {
    save("gradient-"+width+"-"+step+".png");
    x = 0;
    step = step +1;
  }
}