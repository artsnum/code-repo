void setup() {
  size(600,800);
  background(220);
  smooth();
}

void draw() {
  //Le système de coordonnées est réinitialisé à chaque début de boucle draw
  
  //point d'origine
  noStroke();
  fill(255,0,0);
  ellipse(0,0,40,40);
  
  //rectangle
  stroke(0);
  fill(255);
  rect(150,50,150,100, 10);
  rect(350,50,150,100, 10);
  
  //transformation simple
  rotate(radians(15)); //conversion en degrés

  //rectangle
  stroke(0);
  fill(255);
  rect(150,50,150,100, 10);
  rect(350,50,150,100, 10);
  
  //Un deuxième rotation, qui va s'additionner à la précédente
  rotate(radians(15)); //conversion en degrés

  //rectangle
  stroke(0);
  fill(255);
  rect(150,50,150,100, 10);
  rect(350,50,150,100, 10);
  
  //Un troisième rotation, qui va s'additionner à la précédente
  rotate(radians(15)); //conversion en degrés

  //rectangle
  stroke(0);
  fill(255);
  rect(150,50,150,100, 10);
  rect(350,50,150,100, 10);
}
