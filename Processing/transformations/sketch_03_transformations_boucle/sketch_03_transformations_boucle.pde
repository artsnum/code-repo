int nombreRot;
float angle;

void setup() {
  size(600,800);
  background(220);
  smooth();
  
  nombreRot = 30;
  angle = 360/nombreRot;
  
}

void draw() {
  translate(mouseX, mouseY);
  coordonnees();
  for(int i=0; i< nombreRot; i++) {
    fill((255/nombreRot)*i);
    rect(20,20,mouseX,mouseY/5, 10);
    rotate(radians(angle));
  }
}
