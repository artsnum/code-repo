void setup() {
  size(800,800);
  background(220);
  smooth();
}

void draw() {
  //La transformation est réinitialisée à chaque début de boucle draw
  
  //point d'origine
  noStroke();
  fill(255,0,0);
  ellipse(0,0,40,40);
  
  //transformation simple
  rotate(radians(15)); //conversion en degrés

  //rectangle
  stroke(0);
  fill(255);
  rect(200,200,200,100, 10);
  rect(600,200,200,100, 10);
}
