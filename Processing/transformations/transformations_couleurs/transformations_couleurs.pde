int nombreRot;
float angle;
float rouge, vert, bleu;
float arrondi;

void setup() {
  size(600,800);
  background(220);
  smooth();
  nombreRot = 30;
  angle = 360/nombreRot;
  arrondi = 10;
}

void draw() {
  translate(mouseX, mouseY);
  coordonnees();
  for(int i=0; i< nombreRot; i++) {
    noStroke();
    rouge = map(mouseX,0,width,0,255);
    vert = map(mouseY,0,height,0,255);
    fill(rouge, vert, (255/nombreRot)*i);
    rect(20,20,mouseX,mouseY/5, arrondi);
    rotate(radians(angle));
  }
}

void keyPressed() {
  save("Landscape-"+frameCount+".png");
}
