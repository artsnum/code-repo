int posY = 0;
int posX = 0;
PImage img = loadImage("https://s-media-cache-ak0.pinimg.com/736x/d2/79/63/d27963548ff5a6a6fcd56c9f7f9db45a--textile-patterns-design-patterns.jpg");
int arraySize = img.width*img.height;
color[] colorArray = new color[arraySize];
int compteur = 0;

void setup() {
  size(736, 736);
  noStroke();
  loadPixels();
  img.loadPixels();
  for (int y = 0; y < height; y++ ) {
    for (int x = 0; x < width; x++ ) {
      int loc = x + y*width;
      float r = red(img.pixels [loc]); 
      float g = green(img.pixels[loc]);
      float b = blue(img.pixels[loc]);
      colorArray[loc] = color(r,g,b);
    }
  }
  updatePixels();
}

void draw() {
  stroke(colorArray[compteur]);
  if ((posX%width == 0) && (posX != 0)) {
    posX = 0;
    posY += 1;
  }
  line(posX,posY,posX+(random(-5,5)),height);
  posX += 1;
  compteur += 1;
}
