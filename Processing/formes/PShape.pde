PShape forme, jaune;

void setup() {
  size(900,900);
  forme = createShape();
  forme.beginShape();
  forme.fill(0);
  forme.vertex(100,100);
  forme.vertex(300, 100);
  forme.vertex(600,230);
  forme.vertex(300,300);
  forme.endShape(CLOSE);
  noStroke();
  jaune = createShape();
  jaune.beginShape();
  jaune.fill(250,230,0);
  jaune.vertex(200,200);
  jaune.vertex(300, 220);
  jaune.vertex(290,700);
  jaune.vertex(180,580);
  jaune.endShape(CLOSE);
}

void draw() {
  background(255);
  for (int i=0; i< forme.getVertexCount();i++) {
    float x = forme.getVertexX(i);
    float y = forme.getVertexY(i);
    x += random(-0.3,0.4);
    y += random(-0.2,0.3);
    forme.setVertex(i,x,y);
  }
  for (int i=0; i< jaune.getVertexCount();i++) {
    float x = jaune.getVertexX(i);
    float y = jaune.getVertexY(i);
    x += random(-0.2,0.2);
    y += random(-0.2,0.2);
    jaune.setVertex(i,x,y);
  }
  shape(jaune,80,30);
  shape(forme,50,50);
}