/* Placer une forme */

void setup() {
  size(740,370);
  background(255);
  noStroke();
  fill(240,50,10,200);
  beginShape();
    vertex(0,0);
    vertex(100,0);
    vertex(100,100);
    vertex(0,100);
  endShape(CLOSE);
}
