/* L'animer */
float x1,y1,x2,y2,x3,y3,x4,y4;

void setup() {
  size(740,370);
  background(255);
  noStroke();
}

void draw() {
  fill(255);
  rect(0,0,width,height);
  x1 = 0;
  y1 = 0;
  x2 = 100;
  y2 = 0;
  x3 = mouseX;//100;
  y3 = mouseY;//100;
  x4 = 0;
  y4 = 100;
  fill(240,50,10,200);
  beginShape();
    vertex(x1,y1);
    vertex(x2,y2);
    vertex(x3,y3);
    vertex(x4,y4);
  endShape(CLOSE);
}
