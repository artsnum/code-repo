// On crée une variable qui correspondra à notre position sur l'axe des X. On lui donne la valeur de départ "0" (à gauche)
int posX = 0;

// On crée une variable qui correspondra à notre position sur l'axe des Y. On lui donne la valeur de départ "0" (en haut)
int posY = 0;

// On défini le côté de nos carrés à l'aide d'une variable qu'on appelle, par exemple "cote"
int cote = 100;

// On crée ici une variable booléenne (de type "vrai ou faux") qui nous servira à intervertir les couleurs de nos carrés. Elle est vraie par défaut.
boolean inverter;

void setup() {
    size(1200,900);
    noStroke(); //On désactive les contours
}

void draw() {
    if (inverter) { // si "inverter" est VRAI (ce qui est le cas par défaut)
        fill(255); // Remplir de blanc
    } else { // Dans les autres cas..
        fill(random(0,200)); //.. remplir d'une valeur aléatoire comprise entre 0 et 200.
    }
    rect(posX, posY, cote, cote);
    posX += cote; //On augmente la position en X de la valeur d'un côté du carré
    //inverter égale le contraire d'inverter. Si elle est fausse, elle devient vraie. Si elle est vraie, elle devient fausse.
    inverter = !inverter;
    // Si la position en X dépasse ou est égale à la largeur du sketch, on passe à la ligne...
    if (posX >= width) {
        posX = 0; // En ramenant X à 0 (bord gauche)
        posY += cote; //En augmentant la position en Y de la hauteur d'un carré
        inverter = !inverter; // On inverse également la couleur. Commentez cette ligne( // ) si nombre impair..
    }
}