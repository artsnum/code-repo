/* Générateur de formes */
float x1,y1,x2,y2,x3,y3,x4,y4,decalage;
int cote = 100;

void setup() {
  size(740,370);
  background(255);
  noStroke();
  frameRate(1);
  decalage = cote/2;
}

void draw() {
  //On place un fond gris clair à chaque boucle
  fill(240);
  rect(0,0,width,height);
  
  //On déplace a forme au centre du sketch
  translate(width/2-cote/2,height/2-cote/2);
  
  //On calcule les nouvelles valeurs des variables
  x1 = 0+random(-decalage,decalage);
  y1 = 0+random(-decalage,decalage);
  x2 = cote+random(-decalage,decalage);
  y2 = 0+random(-decalage,decalage);
  x3 = cote+random(-decalage,decalage);
  y3 = cote+random(-decalage,decalage);
  x4 = 0+random(-decalage,decalage);
  y4 = cote+random(-decalage,decalage);
  
  //On définit une couleur de remplissage (qui pourrait être différente à chaque boucle)
  fill(240,50,10,200);
  
  //On dessine la forme (qui pourrait d'ailleurs être bien plus complexe que ceci)
  beginShape();
    vertex(x1,y1);
    vertex(x2,y2);
    vertex(x3,y3);
    vertex(x4,y4);
  endShape(CLOSE);
  
  //La ligne suivante, décommentée, exportera un fichier JPG de chaque forme dans le dossier du sketch, à chaque boucle donc.
  //saveFrame("maForme-#####.jpg");
}
