float diametre = 20;

void setup() {
  size(800,600);
  ellipseMode(CENTER);
  diametre = 20;
  noStroke();
  translate(diametre, diametre);
  for(int j=0;j<height-diametre;j+=diametre) {
    for(int i=0;i<width-diametre;i+=diametre) {
      ellipse(i, j, diametre, diametre);
    }
  }
}
