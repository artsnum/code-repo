int largeur; 
int diviseur;

void setup(){
  size(500,800); 
  background(0,0,0);
  noStroke();
  frameRate(2);
  largeur = width/5;
  diviseur = 6;
}

void draw() {
  for(int j=0;j<5;j++) {
    for(int i=0;i<5;i++) {
      diviseur = floor(random(10));
      Grille(i*largeur,j*largeur,largeur,largeur,diviseur,diviseur);
    }
  }
}

void Grille(float posx, float posy, float largeur, float hauteur, int nbrL, int nbrH) {
  float coteL = largeur/nbrL;
  float coteH = largeur/nbrH;
  for(int j=0;j<nbrH;j++) {
    for(int i=0;i<nbrL;i++) {
      fill(random(0,100),random(50,150),random(100,250));
      rect(posx+(i*coteL), posy+(j*coteH), coteL, coteH);
    }
  }
}
