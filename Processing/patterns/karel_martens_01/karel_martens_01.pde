/* Inspired by Karel Martens*,Untitled, 2014*/
/* https://www.sfmoma.org/artwork/2017.309/ */

color magenta = color(201, 14, 28,200);
color jaune = color(238, 178, 41,200);
color cyan = color(19, 133, 176,200);
int dia;
int nbrX, nbrY;
float rand, angle;
float deg, degX;
  float rad;

void setup() {
  size(1020,695);
  noStroke();
  //frameRate(15);
  ellipseMode(CENTER);
  nbrX = 40;
  nbrY = 40;
  dia = 10;
  blendMode(MULTIPLY);
  angle = 2;
  background(255);
}

void draw() {
  background(255);
  deg = 5;
  degX = mouseX;
  rad = radians(deg);
  pushMatrix();
  rotate(radians(degX));
  for (int y = dia; y < height-dia; y = y + dia*2) {
    for (int x = dia/2; x < width-dia; x = x + dia*2) {
      fill(magenta);
      ellipse(x,y,dia, dia);
    }
  }
  popMatrix();
  rotate(rad*-0.2);
  for (int y = dia; y < height-dia; y = y + dia*2) {
    for (int x = dia; x < width-dia; x = x + dia*2) {
      fill(jaune);
      ellipse(x,y,dia, dia);
    }
  }
  rotate(rad*.5);
  for (int y = dia; y < height-dia; y = y + dia*2) {
    for (int x = dia; x < width-dia; x = x + dia*2) {
      fill(cyan);
        ellipse(x,y,dia, dia);
    }
  }
  //
  
  
}
