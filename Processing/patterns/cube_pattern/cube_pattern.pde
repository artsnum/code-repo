int nbr = 20;
float cote, var, posx, posy, decalage;

void setup() {
  size(800,800);
  noStroke();
  cote = width/nbr;
  var = cote/4;
  posx = 0;
  posy = -var;
  background(20);
  //frameRate(2);
}

void draw() {
  /*var = mouseX/10;
  cote = mouseY/3;*/
  cube(
    posx,
    posy,
    cote,
    var,
    color(random(200,255),0,0),
    color(0,random(100,240),0),
    color(random(0,40),0,random(0,200))
  );
  posx += cote*2;
  if(posx >= width) {
    if(decalage == 0) {
      decalage = -cote;
    } else {
      decalage = 0;
    }
    posx = 0+decalage;
    posy += cote+var;
  }
  if(posy >= height) {
    posy = -var;
  }
}


void cube(float posx, float posy, float cote, float var, color color1, color color2, color color3) {
  fill(color1);
  beginShape();
    vertex(posx, posy+var);
    vertex(posx+cote, posy);
    vertex(posx+cote, posy+cote);
    vertex(posx, posy+cote+var);
  endShape(CLOSE);
  fill(color2);
  beginShape();
    vertex(posx+cote, posy);
    vertex(posx+cote*2, posy+var);
    vertex(posx+cote*2, posy+cote+var);
    vertex(posx+cote, posy+cote);
  endShape(CLOSE);
  fill(color3);
  beginShape();
    vertex(posx+cote, posy+cote);
    vertex(posx+cote*2, posy+cote+var);
    vertex(posx+cote, posy+cote+var*2);
    vertex(posx, posy+cote+var);
  endShape(CLOSE);
}
