void setup() {
  size(1200,900);
  background(200);
  noStroke();
}

void draw() {
  loadPixels();
  for(int x=0; x<width; x++) {
    for(int y = 0; y<height;y++) {
      int index = x+y*width;
      pixels[index] = color(index/height/4,random(200),random(125));
    }
  }
  updatePixels();
  fill(255,0,10,10);
  for(int i=0;i<10;i++) {
    rect(20*i,20*i,width-(20*i)*2,height-(20*i)*2);
  }
}
