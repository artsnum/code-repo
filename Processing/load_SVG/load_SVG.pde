/* Déclarer une variablé spécifique de type PShape */
PShape lines;

void setup() {
  size(800, 600);
  /* Charger le SVG et l'assigner à la variable */
  lines = loadShape("loumi.svg");
  background(255);
  /* Placera les formes à partir de leur centre */
  shapeMode(CENTER);
} 

void draw(){
  //Place un rectangle blanc avec opacité réduite (pour créer la trace) */
  fill(255,10);
  rect(0,0,width,height);
  
  //Place les formes
  shape(lines, 250, 200, 400, 400); 
  shape(lines, 200, 400);
  /* On utilise ici les variables mouseX et mouseY pour gérer taille et position*/
  shape(lines, mouseX, mouseY,mouseX,mouseX);
}
