PImage img; // déclaration de la variable image

void setup() {
    size(600, 600);    
    // L'image doit se situer dans un dossier appelé data dans le dossier de votre sketch Processing.
    img = loadImage("workshop.jpg"); // attention, pas de caractères spéciaux ni accents ni espaces
}

void draw() {
    image(img, 0, 0); // on affiche l'image en haut à gauche de la page.
}